import io.datafab.GenericConfigFile._
import io.datafab.Main.{main, readData}
import io.datafab.SchemaConfig
import io.circe.generic.auto._

import java.io.File


class MainTest extends UnitTestBase {

  test("readData") {
    val configFile = getConfigFile[SchemaConfig](s"$configPath/read_config.json")
    val df = readData(configFile.viewProcess.head.inputs("df"))
    val df_empty = readData(configFile.viewProcess.head.inputs("df_empty"))
    val df_random = readData(configFile.viewProcess.head.inputs("df_random"))
//    val df_fail = readData(configFile.inputs("df_fail"))
    df_random.printSchema()
    df_random.show(false)
  }

  test("getSchemaFromJson") {
    val schema_list = getSchemaFromJson(s"$schemaPath/schema_list.json")
    val schema_struct = getSchemaFromJson(s"$schemaPath/schema_struct.json")
    val schema_list_struct = getSchemaFromJson(s"$schemaPath/schema_list_struct.json")
    val schema_struct_list = getSchemaFromJson(s"$schemaPath/schema_struct_list.json")
    println(schema_list)
    println(schema_struct)
    println(schema_list_struct)
    println(schema_struct_list)
  }

  test("Transformation select") {
    val path = s"$configPath/select_config.json"
    main(Array(path))
    val configFile = getConfigFile[SchemaConfig](path)
    val result = readData(configFile.viewProcess.head.outputs("df"))
    result.show(false)
  }

  after {
    deleteRecursively(new File(s"$resultsPath/df_select"))
  }

}
