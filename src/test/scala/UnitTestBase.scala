import io.datafab.{GenericLogger, GenericSpark}
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.scalatest.{BeforeAndAfter, FunSuite, Matchers}

import java.io.File

trait UnitTestBase extends FunSuite with BeforeAndAfter with Matchers with GenericLogger {

  val sparkConf: SparkConf = new SparkConf()
    .set("spark.sql.autoBroadcastJoinThreshold", "-1")
    .set("spark.sql.shuffle.partitions", "10")
  implicit val spark: SparkSession = SparkSession.builder
    .master("local[*]")
    .appName("Generic Spark Process UnitTests")
    .config(sparkConf)
    .getOrCreate()
  spark.sparkContext.setLogLevel("ERROR")


  val configPath: String = getClass.getClassLoader.getResource("config").getPath
  val dataPath: String = getClass.getClassLoader.getResource("data").getPath
  val schemaPath: String = getClass.getClassLoader.getResource("schema").getPath
  val resultsPath: String = getClass.getClassLoader.getResource("results").getPath

  def deleteRecursively(file: File): Unit = {
   if (file.isDirectory) {
     file.listFiles.foreach(deleteRecursively)
   }
   if (file.exists && !file.delete) {
     throw new Exception(s"Impossible to delete file: ${file.getAbsoluteFile}")
   }
  }
}
