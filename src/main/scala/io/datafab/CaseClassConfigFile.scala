package io.datafab

case class IOput(path: Option[String],
                 options: Option[Map[String, String]],
                 format: Option[String],
                 partitionBy: Option[List[String]],
                 coalesce: Option[Int],
                 mode: Option[String],
                 lookBackWindowInDays: Option[Int],
                 query: Option[String],
                 cmd: Option[String],
                 schemaPath: Option[String])
case class TransformationSelectParams(columns: List[String], distinct: Option[Boolean])
case class MapKeyParams(leftKey: String, rightKey: String)
case class TransformationJoinParams(tableRight: String, mappingKeys: List[MapKeyParams], joinType: String)
case class TransformationFilterParams(filterExpr: String)
case class TransformationDropParams(columns: List[String])
case class WithColumnWindow(newColumn: String, function: String, fromColumn: String)
case class TransformationWindowParams(partitionBy: String, orderBy: String, withColumns: List[WithColumnWindow])
case class AggregationParams(function: String, column: String, alias: Option[String])
case class TransformationGroupByParams(columns: List[String], aggregations: List[AggregationParams])
case class TransformationAggParams(aggregations: List[AggregationParams])
case class TransformationRenameParams(renamingColumns: Map[String, String])
case class TransformationCreateParams(newColumnName: String, columnType: Option[String], columnName: Option[String], functionName: String, value: Option[String], newValue: Option[String], pattern: Option[String], index: Option[Int], columns: Option[List[String]], schemaPath: Option[String], dateFormat: Option[String], lookBackWindowInDays: Option[Int])
case class LookupPrioritiesParams(filterExpr: Option[String], joinConditions: Map[String, String])
case class LookupConditionsParams(searchColumn: String, lookupPriorities: List[LookupPrioritiesParams])
case class TransformationLookupParams(lookupTable: String, lookupConditions: LookupConditionsParams, returnColumn: String)
case class TransformationExceptParams(otherTable: String)
case class TransformationUnionParams(otherTable: String)
case class TransformationUpdateTableParams(oldTable: String, columnsToCompare: List[String], columnsToAdd: Option[List[String]], updateColumns: List[TransformationCreateParams])
case class TransformationParams(transformationOn: String,
                                newTableName: Option[String],
                                columns: Option[List[String]],
                                distinct: Option[Boolean],
                                leftKey: Option[String],
                                rightKey: Option[String],
                                rightTable: Option[String],
                                leftTable: Option[String],
                                mappingKeys: Option[String],
                                joinType: Option[String],
                                filterExpr: Option[String],
                                partitionBy: Option[String],
                                orderBy: Option[String],
                                withColumns: Option[List[WithColumnWindow]],
                                aggregations: Option[List[AggregationParams]],
                                renamingColumns: Option[Map[String, String]],
                                joinConditions: Option[Map[String, String]],
                                searchColumn: Option[String],
                                otherTable: Option[String],
                                oldTable: Option[String])
case class Transformation(
                           transformationType: String,
                           transformationOn: String,
                           transformationParams: Option[TransformationParams],
                           transformationFilter: Option[TransformationFilterParams],
                           transformationJoin: Option[TransformationJoinParams],
                           transformationSelect: Option[TransformationSelectParams],
                           transformationDrop: Option[TransformationDropParams],
                           transformationRename: Option[TransformationRenameParams],
                           transformationCreate: Option[TransformationCreateParams],
                           transformationGroupBy: Option[TransformationGroupByParams],
                           transformationWindow: Option[TransformationWindowParams],
                           transformationAgg: Option[TransformationAggParams],
                           transformationLookup: Option[TransformationLookupParams],
                           transformationIntersect: Option[TransformationExceptParams],
                           transformationExcept: Option[TransformationExceptParams],
                           transformationUnion: Option[TransformationUnionParams],
                           transformationUpdateTable: Option[TransformationUpdateTableParams],
                           newTableName: Option[String]
                         )
case class ViewProcess(inputs: Map[String, IOput], controls: Option[String], transformations: Array[Transformation], outputs: Map[String, IOput])
case class SchemaConfig(viewProcess: List[ViewProcess])