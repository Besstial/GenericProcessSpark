package io.datafab

import org.apache.spark.sql.SparkSession

trait GenericSpark extends GenericLogger {
  implicit val spark: SparkSession = SparkSession.builder.appName("Generic Spark").getOrCreate()
  logger.info(s"Launch SparkSession")
}