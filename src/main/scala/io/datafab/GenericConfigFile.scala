package io.datafab

import org.apache.hadoop.fs.{FileSystem, Path}
import io.circe.parser.decode
import org.apache.spark.sql.types.DataType

import java.time.LocalDate

object GenericConfigFile extends GenericSpark {

  def readFile(filePath: String): String = {
    logger.info(s"Reading file at $filePath")
    val fs = FileSystem.get(spark.sparkContext.hadoopConfiguration)
    val configFileInputStream = fs.open(new Path(filePath))
    scala.io.Source.fromInputStream(configFileInputStream).mkString
  }

  def getConfigFile[T](filePath: String)(implicit decoder: io.circe.Decoder[T]): T = {
    decode[T](readFile(filePath)) match {
      case Right(jsonConfig) => jsonConfig
      case Left(jsonConfig)  => throw new Exception(jsonConfig)
    }
  }

  def getSchemaFromJson(filePath: String): DataType = {
    DataType.fromJson(readFile(filePath))
  }

  def getPathWithDate(filePath: String, lookBackWindowInDays: Option[Int]): String = {
    val date = LocalDate.now().plusDays(lookBackWindowInDays.getOrElse(0).toLong)
    filePath
      .replace("YYYY", date.getYear.toString)
      .replace("YY", date.getYear.toString.substring(2))
      .replace("MM", date.getMonthValue.toString.reverse.padTo(2, "0").reverse.toString)
      .replace("DD", date.getDayOfMonth.toString.reverse.padTo(2, "0").reverse.toString)
  }
}