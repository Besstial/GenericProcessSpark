package io.datafab

import org.apache.spark.sql.DataFrame

case class DataFrameWithName(tableName: String, dataFrame: DataFrame) {
  override def toString: String = {
    s"show header for table: $tableName ... ${dataFrame.show(false)}"
  }
}