package io.datafab

import org.slf4j.{Logger, LoggerFactory}

trait GenericLogger {
  implicit val logger: Logger = LoggerFactory.getLogger("Generic Logger")
  logger.info(s"Welcome to Generic Spark Process application!")
}