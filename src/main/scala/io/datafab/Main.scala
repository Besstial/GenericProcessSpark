package io.datafab

import io.circe.generic.auto._
import io.datafab.GenericConfigFile.{getConfigFile, getPathWithDate, getSchemaFromJson}
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.{AnalysisException, Column, DataFrame, DataFrameReader, DataFrameWriter, Row}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.StructType

import java.security.KeyException
import scala.collection.mutable
import scala.sys.process.Process

object Main extends GenericSpark with GenericLogger {

  val dataFramesWithName: mutable.MutableList[DataFrameWithName] = mutable.MutableList.empty[DataFrameWithName]
  def main(args: Array[String]): Unit = {
    val config: SchemaConfig = getConfigFile[SchemaConfig](args(0))
    startProcess(config)
  }

  def startProcess(config: SchemaConfig): Unit = {
    logger.info(s"Start looping process")
    for (viewProcess <- config.viewProcess) {
      doProcess(viewProcess)
    }
  }

  def doProcess(viewProcess: ViewProcess): Unit = {
    readDatas(viewProcess.inputs)
    doControls(viewProcess.controls)
    createPlan(viewProcess.transformations)
    writeDatas(viewProcess.outputs)
  }

  def manageMutableListDataFrames(dataFrame: DataFrame, name: String): Unit = {
    val index = dataFramesWithName.indexWhere(_.tableName == name)
    index match {
      case -1 => dataFramesWithName += DataFrameWithName(name, dataFrame)
      case _ => dataFramesWithName.update(index, DataFrameWithName(name, dataFrame))
    }
  }

  def getDataFrameByName(name: String): DataFrame = {
    dataFramesWithName.find(_.tableName == name) match {
      case Some(df) => df.dataFrame
      case None => throw new KeyException(name)
    }
  }

  def readDatas(inputs: Map[String, IOput]): Unit = {
    for ((tableName, input) <- inputs) {
      manageMutableListDataFrames(readData(input), tableName)
    }
  }
  def readData(input: IOput): DataFrame = {
    val format = input.format.getOrElse("parquet")
    format match {
      case "sql" => spark.sql(getPathWithDate(input.query.get, input.lookBackWindowInDays))
      case _ => try {
        addSchema(spark.read.options(input.options.getOrElse(Map())).format(format), input.schemaPath)
          .load(getPathWithDate(input.path.get, input.lookBackWindowInDays))
      } catch {
        case _: AnalysisException => {
          logger.info(s"Path doesn't exist: ${input.path.get}. Try to create Empty DataFrame with specific schema")
          spark.createDataFrame(spark.sparkContext.emptyRDD[Row], getSchemaFromJson(input.schemaPath.get).asInstanceOf[StructType])
        }
      }
    }
  }

  def addSchema(reader: DataFrameReader, schema: Option[String]): DataFrameReader = {
    schema match {
      case Some(schema) => reader.schema(getSchemaFromJson(schema).asInstanceOf[StructType])
      case None => reader
    }
  }

  def doControls(controls: Option[String]): Unit = {
    logger.info(s"No ${controls.getOrElse("controls")} yet implemented")
  }

  def createPlan(transformations: Array[Transformation]): Unit = {
    for (transformation <- transformations) {
      val dfBefore = getDataFrameByName(transformation.transformationOn)
      val dfAfter = transformation.transformationType match {
        case "select" => addTransformationSelect(dfBefore, transformation.transformationSelect.get)
        case "distinct" => addTransformationDistinct(dfBefore, transformation.transformationParams.get.distinct)
        case "join" => addTransformationJoin(DataFrameWithName(transformation.transformationOn, dfBefore), transformation.transformationJoin.get)
        case "drop" => addTransformationDrop(dfBefore, transformation.transformationDrop.get)
        case "rename" => addTransformationRename(dfBefore, transformation.transformationRename.get)
        case "filter" => addTransformationFilter(dfBefore, transformation.transformationFilter.get)
        case "create" => addTransformationCreate(dfBefore, transformation.transformationCreate.get)
        case "groupby" => addTransformationGroupBy(dfBefore, transformation.transformationGroupBy.get)
        case "window" => addTransformationWindow(dfBefore, transformation.transformationWindow.get)
        case "agg" => addTransformationAgg(dfBefore, transformation.transformationAgg.get)
        case "lookup" => addTransformationLookup(dfBefore, transformation.transformationLookup.get)
        case "intersect" => addTransformationIntersect(dfBefore, transformation.transformationIntersect.get)
        case "except" => addTransformationExcept(dfBefore, transformation.transformationExcept.get)
        case "updateTable" => addTransformationUpdateTable(dfBefore, transformation.transformationUpdateTable.get)
        case "union" => addTransformationUnion(dfBefore, transformation.transformationUnion.get)
        case "show" => dfBefore.show(); dfBefore
        case "printSchema" => dfBefore.printSchema(); dfBefore
        case "cache" => dfBefore.cache()
        case "copy" => dfBefore
        case _ => throw new KeyException(transformation.transformationType)
      }
      manageMutableListDataFrames(dfAfter, transformation.newTableName.getOrElse(transformation.transformationOn))
    }
  }

  def addTransformationUnion(dataFrame: DataFrame, transformationUnionParams: TransformationUnionParams): DataFrame = {
    dataFrame.union(getDataFrameByName(transformationUnionParams.otherTable))
  }

  def addTransformationUpdateTable(newDataframe: DataFrame, transformationUpdateTableParams: TransformationUpdateTableParams): DataFrame = {
    val oldDataframe = getDataFrameByName(transformationUpdateTableParams.oldTable)

    val columnsToCompare = transformationUpdateTableParams.columnsToCompare
    val newDataframeSelect = newDataframe.select(columnsToCompare.head, columnsToCompare.tail: _*)
    val oldDataframeSelect = oldDataframe.select(columnsToCompare.head, columnsToCompare.tail: _*)

    val dataframeIntersect = newDataframeSelect.intersect(oldDataframeSelect)
    val dataframeExcept = newDataframeSelect.except(oldDataframeSelect)

    val updatedColumnsInNewDF = transformationUpdateTableParams.updateColumns.foldLeft(dataframeExcept) {
      (dataFrame, createParams) => addTransformationCreate(dataFrame, createParams)
    }

    val updateColumnsInOldDF = dataframeIntersect.na.fill("TEMPORARY").distinct()
      .join(oldDataframe.na.fill("TEMPORARY").distinct(), columnsToCompare, "left")
      .select(columnsToCompare.head, columnsToCompare.tail ::: transformationUpdateTableParams.updateColumns.map(_.newColumnName): _*)

    transformationUpdateTableParams.columnsToAdd match {
      case Some(columnsToAdd) => updatedColumnsInNewDF.union(updateColumnsInOldDF).na.fill("TEMPORARY").distinct()
        .join(newDataframe.na.fill("TEMPORARY").select(columnsToCompare.head, columnsToCompare.tail ::: columnsToAdd: _*).distinct(), columnsToCompare, "left")
        .na.replace("*", Map("TEMPORARY" -> null))
        .select(columnsToCompare.head, columnsToCompare.tail ::: transformationUpdateTableParams.updateColumns.map(_.newColumnName) ::: columnsToAdd: _*)
      case None => updatedColumnsInNewDF.union(updateColumnsInOldDF).na.replace("*", Map("TEMPORARY" -> null))
    }
  }

  def addTransformationExcept(dataFrame: DataFrame, transformationExceptParams: TransformationExceptParams): DataFrame = {
    val otherDataframe = getDataFrameByName(transformationExceptParams.otherTable)
    val commonColumns = dataFrame.columns.intersect(otherDataframe.columns)
    dataFrame.select(commonColumns.head, commonColumns.tail: _*).except(otherDataframe.select(commonColumns.head, commonColumns.tail: _*))
  }

  def addTransformationIntersect(dataFrame: DataFrame, transformationExceptParams: TransformationExceptParams): DataFrame = {
    val otherDataframe = getDataFrameByName(transformationExceptParams.otherTable)
    val commonColumns = dataFrame.columns.intersect(otherDataframe.columns)
    dataFrame.select(commonColumns.head, commonColumns.tail: _*).intersect(otherDataframe.select(commonColumns.head, commonColumns.tail: _*))
  }

  def addTransformationLookup(dataFrame: DataFrame, transformationLookupParams: TransformationLookupParams): DataFrame = {
    val lookupName = transformationLookupParams.lookupTable
    val lookupTable = getDataFrameByName(lookupName)
    var index = 0
    val newDF = transformationLookupParams.lookupConditions.lookupPriorities.foldLeft(dataFrame) {
      (dataFrame, lookupPriority) => {
        val lookupTableInit = (lookupPriority.filterExpr match {
          case Some(filterExpr) => lookupTable.filter(filterExpr)
          case _ => lookupTable
      })
          .select(transformationLookupParams.lookupConditions.searchColumn, lookupPriority.joinConditions.values.filter(_ != transformationLookupParams.lookupConditions.searchColumn).toList: _*)
          .withColumn("search_column", col(transformationLookupParams.lookupConditions.searchColumn))
          .withColumnRenamed("search_column", s"${lookupName}_lookup_result_$index").distinct()
        val filterLookupTable = lookupPriority.joinConditions.values.toList.foldLeft(lookupTableInit)(
          (df, columnName) => df.withColumnRenamed(columnName, s"left_$columnName")
        ).drop(transformationLookupParams.lookupConditions.searchColumn)
        index += 1
        val conditions = lookupPriority.joinConditions.map(condition => lower(dataFrame(condition._1)) === lower(filterLookupTable(s"left_${condition._2}"))).reduce(_ && _)
        dataFrame.join(filterLookupTable, conditions, "left").drop(lookupPriority.joinConditions.values.map(colName => s"left_$colName").toList: _*)
      }
    }
    val cols = newDF.columns.filter(_.startsWith(s"${lookupName}_lookup_result_"))
    newDF.withColumn(transformationLookupParams.returnColumn, coalesce(cols.map(col): _*)).drop(cols: _*)
  }

  private def convertStringToFunction(functionName: String): String => Column = {
    functionName match {
      case "sum" => (col: String) => sum(col)
      case "max" => (col: String) => max(col)
      case "min" => (col: String) => min(col)
      case "avg" => (col: String) => avg(col)
      case "count" => (col: String) => count(col)
      case "rank" => _ => row_number()
      case "collect_set" => (col: String) => collect_set(col)
      case "collect_list" => (col: String) => collect_list(col)
      case "countDistinct" => (col: String) => countDistinct(col)
      case "first_ignore_null" => (col: String) => first(col, ignoreNulls = true)
      case _ => throw new IllegalArgumentException("Aggregation function unknow")
    }
  }

  private def addAliasToColumn(column: Column, alias: Option[String]): Column = {
    alias match {
      case Some(name) => column.alias(name)
      case _ => column
    }
  }

  def addTransformationWindow(dataFrame: DataFrame, transformationWindowParams: TransformationWindowParams): DataFrame = {
    val window = Window.partitionBy(transformationWindowParams.partitionBy).orderBy(transformationWindowParams.orderBy)
    transformationWindowParams.withColumns.foldLeft(dataFrame) {
      (dataFrame, withColumn) => dataFrame.withColumn(withColumn.newColumn, convertStringToFunction(withColumn.function)(withColumn.fromColumn).over(window))
    }
  }

  def addTransformationAgg(dataFrame: DataFrame, transformationAggParams: TransformationAggParams): DataFrame = {
    val aggregations = transformationAggParams.aggregations.map(agg => addAliasToColumn(convertStringToFunction(agg.function)(agg.column), agg.alias))
    dataFrame.agg(aggregations.head, aggregations.tail: _*)
  }

  def addTransformationGroupBy(dataFrame: DataFrame, transformationGroupByParams: TransformationGroupByParams): DataFrame = {
    val aggregations = transformationGroupByParams.aggregations.map(agg => addAliasToColumn(convertStringToFunction(agg.function)(agg.column), agg.alias))
    dataFrame.groupBy(transformationGroupByParams.columns.map(col): _*).agg(aggregations.head, aggregations.tail: _*)

  }

  def addTransformationDistinct(dataFrame: DataFrame, bool: Option[Boolean]): DataFrame = {
    bool match {
      case Some(true) => dataFrame.distinct()
      case _ => dataFrame
    }
  }

  def addTransformationSelect(dataFrame: DataFrame, transformationSelectParams: TransformationSelectParams): DataFrame = {
    addTransformationDistinct(dataFrame.select(transformationSelectParams.columns.map(col): _*), transformationSelectParams.distinct)
  }

  def addTransformationDrop(dataFrame: DataFrame, transformationDropParams: TransformationDropParams): DataFrame = {
    dataFrame.drop(transformationDropParams.columns: _*)
  }

  def addTransformationRename(dataFrame: DataFrame, transformationRenameParams: TransformationRenameParams): DataFrame = {
    transformationRenameParams.renamingColumns.foldLeft(dataFrame) {
      (dataFrame, renameCols) => dataFrame.withColumnRenamed(renameCols._1, renameCols._2)
    }
  }

  def addTransformationFilter(dataFrame: DataFrame, transformationFilterParams: TransformationFilterParams): DataFrame = {
    dataFrame.filter(transformationFilterParams.filterExpr)
  }

  private def renameColumns(dataFrameWithName: DataFrameWithName, duplicateColumns: Set[String]): DataFrameWithName = {
    val renamedColumns = dataFrameWithName.dataFrame.columns.map {
      case col if duplicateColumns.contains(col) => s"${dataFrameWithName.tableName}_$col"
      case col => col
    }
    DataFrameWithName(dataFrameWithName.tableName, dataFrameWithName.dataFrame.toDF(renamedColumns: _*))
  }

  private def renameDuplicateColumnsBetweenDataFrames(dataFrameWithNames: List[DataFrameWithName], excludeColumns: Set[String]): List[DataFrameWithName] = {
    val allColumns = dataFrameWithNames.flatMap(_.dataFrame.columns)
    val duplicatedColumns = allColumns.groupBy(identity).collect {
      case (x, ys) if ys.lengthCompare(1) > 0 && !excludeColumns.contains(x) => x
    }.toSet[String]
    dataFrameWithNames.map(df => renameColumns(df, duplicatedColumns))
  }

  def addTransformationJoin(dataFrameWithName: DataFrameWithName, transformationJoinParams: TransformationJoinParams): DataFrame = {
    val otherDataFrame = DataFrameWithName(transformationJoinParams.tableRight, getDataFrameByName(transformationJoinParams.tableRight))
    val excludeRenameColumns = transformationJoinParams.mappingKeys.flatMap { actionJoinParams => List(actionJoinParams.leftKey, actionJoinParams.rightKey)}.toSet[String]
    val listDataFrameWithNames = renameDuplicateColumnsBetweenDataFrames(List(dataFrameWithName, otherDataFrame), excludeRenameColumns)
    val leftDataFrame = listDataFrameWithNames.head.dataFrame
    val rightDataFrame = listDataFrameWithNames.last.dataFrame

    val resultDataFrame = leftDataFrame.join(
      rightDataFrame,
      transformationJoinParams.mappingKeys.map { x => leftDataFrame(x.leftKey) === rightDataFrame(x.rightKey) }.reduce(_ && _),
      transformationJoinParams.joinType)
    transformationJoinParams.mappingKeys.foldLeft(resultDataFrame){(dataFrame, mappingKey) => {
      mappingKey.rightKey match {
        case mappingKey.leftKey => dataFrame.drop(rightDataFrame(mappingKey.rightKey))
        case _ => dataFrame
      }
    }}
  }

  def addTransformationCreate(dataFrame: DataFrame, transformationCreateParams: TransformationCreateParams): DataFrame = {
    transformationCreateParams.functionName match {
      case "lit" => litCreate(dataFrame, transformationCreateParams.newColumnName, transformationCreateParams.value.getOrElse(""), transformationCreateParams.columnType.getOrElse("string"))
      case "duplicate" => duplicateCreate(dataFrame, transformationCreateParams.newColumnName, transformationCreateParams.columnName.getOrElse(""), transformationCreateParams.columnType)
      case "expr" => exprCreate(dataFrame, transformationCreateParams.newColumnName, transformationCreateParams.value.getOrElse(""), transformationCreateParams.columnType.getOrElse("string"))
      case "extract" => extractCreate(dataFrame, transformationCreateParams.newColumnName, transformationCreateParams.columnName.get, transformationCreateParams.value.getOrElse(""), transformationCreateParams.index.getOrElse(0), transformationCreateParams.columnType.getOrElse("string"))
      case "replace" => replaceCreate(dataFrame, transformationCreateParams.newColumnName, transformationCreateParams.columnName.get, transformationCreateParams.value.getOrElse(""), transformationCreateParams.newValue.getOrElse(0), transformationCreateParams.columnType.getOrElse("string"))
      case "date" => dateCreate(dataFrame, transformationCreateParams.newColumnName, transformationCreateParams.lookBackWindowInDays.getOrElse(0), transformationCreateParams.dateFormat.getOrElse("yyyy-MM-dd"))
      case "split" => splitCreate(dataFrame, transformationCreateParams.newColumnName, transformationCreateParams.columnName.getOrElse(""), transformationCreateParams.columnType.getOrElse("string"), transformationCreateParams.pattern.getOrElse(""), transformationCreateParams.index)
      case "input_file_name" => inputFileNameCreate(dataFrame, transformationCreateParams.newColumnName)
      case "coalesce" => coalesceCreate(dataFrame, transformationCreateParams.newColumnName, transformationCreateParams.columns.get)
      case "from_json" => fromJsonCreate(dataFrame, transformationCreateParams.newColumnName, transformationCreateParams.columnName.get, transformationCreateParams.schemaPath.get)
      case "explode" => explodeCreate(dataFrame, transformationCreateParams.newColumnName, transformationCreateParams.columnName.get)
      case _ => logger.error(transformationCreateParams.functionName); throw new IllegalArgumentException()
    }
  }

  private def addCast(col: Column, columnType: Option[String]): Column = {
    columnType match {
      case Some(colType) => col.cast(colType)
      case _ => col
    }
  }

  private def coalesceCreate(dataFrame: DataFrame, newColumnName: String, columns: List[String]): DataFrame = {
    dataFrame.withColumn(newColumnName, coalesce(columns.map(col): _*).as("String"))
  }

  private def splitCreate(dataFrame: DataFrame, newColumnName: String, columnName: String, columnType: String, pattern: String, index: Option[Int]): DataFrame = {
    val splitColumnDF = dataFrame.withColumn(newColumnName, split(col(columnName), pattern))
    index match {
      case Some(nb) => splitColumnDF.withColumn(newColumnName, col(newColumnName).getItem(nb).cast(columnType))
      case _ => splitColumnDF
    }
  }

  private def inputFileNameCreate(dataFrame: DataFrame, newColumnName: String): DataFrame = {
    dataFrame.withColumn(newColumnName, input_file_name())
  }

  private def duplicateCreate(dataFrame: DataFrame, newColumnName: String, columnName: String, columnType: Option[String]): DataFrame = {
    dataFrame.withColumn(newColumnName, addCast(col(columnName), columnType))
  }

  private def dateCreate(dataFrame: DataFrame, columnName: String, lookBackWindowInDays: Int, dateFormat: String): DataFrame = {
    dataFrame.withColumn(columnName, date_format(date_add(current_date(), lookBackWindowInDays), dateFormat))
  }

  private def litCreate(dataFrame: DataFrame, columnName: String, value: String, columnType: String): DataFrame = {
    dataFrame.withColumn(columnName, lit(value).cast(columnType))
  }

  private def exprCreate(dataFrame: DataFrame, columnName: String, value: String, columnType: String): DataFrame = {
    dataFrame.withColumn(columnName, expr(value).cast(columnType))
  }

  private def fromJsonCreate(dataFrame: DataFrame, newColumnName: String, columnName: String, schemaPath: String): DataFrame = {
    dataFrame.withColumn(newColumnName, from_json(col(columnName), getSchemaFromJson(schemaPath)))
  }

  private def explodeCreate(dataFrame: DataFrame, newColumnName: String, colmnName: String): DataFrame = {
    dataFrame.withColumn(newColumnName, explode(col(colmnName)))
  }

  private def extractCreate(dataFrame: DataFrame, columnName: String, columnNameOrigin: String, value: String, index: Int, columnType: String): DataFrame = {
    dataFrame.withColumn(columnName, regexp_extract(col(columnNameOrigin), value, index).cast(columnType))
  }

  private def replaceCreate(dataFrame: DataFrame, columnName: String, columnNameOrigin: String, value: String, newValue: String, columnType: String) : DataFrame = {
    dataFrame.withColumn(columnName, regexp_replace(col(columnNameOrigin), value, newValue).cast(columnType))
  }
  def writeDatas(outputs: Map[String, IOput]): Unit = {
    for ((tableName, output) <- outputs) {
      writeData(getDataFrameByName(tableName), output)
    }
  }
  def writeData(dataFrame: DataFrame, output: IOput): Unit = {
    output.format.get match {
      case "cmd" => LaunchCMD(output)
      case "sql" => spark.sql(getPathWithDate(output.query.get, output.lookBackWindowInDays))
      case _ => addPartitionBy(addCoalesce(addMissingColumns(dataFrame, output.schemaPath), output.coalesce).write, output.partitionBy)
      .options(output.options.getOrElse(Map()))
      .mode(output.mode.getOrElse("append"))
      .format(output.format.getOrElse("parquet"))
      .save(getPathWithDate(output.path.get, output.lookBackWindowInDays))
    }
  }

  def addMissingColumns(dataFrame: DataFrame, schemaPath: Option[String]): DataFrame = {
    val missingColumns = schemaPath match {
      case Some(schemaPath) => getSchemaFromJson(schemaPath).asInstanceOf[StructType].fields.filterNot(f => dataFrame.columns.toSet.contains(f.name))
      case None => StructType(Seq()).fields
    }
    missingColumns.foldLeft(dataFrame) {
      (df, column) => df.withColumn(column.name, lit(null).cast(column.dataType))
    }
  }

  def addCoalesce(dataFrame: DataFrame, coalesce: Option[Int]): DataFrame = {
    coalesce match {
      case Some(nb) => dataFrame.coalesce(nb)
      case _ => dataFrame
    }
  }

  def addPartitionBy(dataFrame: DataFrameWriter[Row], partitionBy: Option[List[String]]): DataFrameWriter[Row] = {
    partitionBy match {
      case Some(cols) => dataFrame.partitionBy(cols: _*)
      case _ => dataFrame
    }
  }

  def LaunchCMD(output: IOput): Unit = {
    val splitCMD = getPathWithDate(output.cmd.get, output.lookBackWindowInDays).split("\\|").map(_.trim)
    val CMD = splitCMD.foldLeft(Process("true")){(cmd, string) => cmd #| string}
    try {
      val outputCMD = CMD.!!
      logger.info(s"$outputCMD")
    } catch  {
      case e: Exception => logger.info(s"Error with CMD: $e")
    }
  }
}


